/* Simple Merge Sort Program Using Functions and Array in C*/


#include<stdio.h>
#include<conio.h>

#define MAX 10

void merge_sort(int, int);
void merge_array(int, int, int, int);

int arr[MAX];

int main() {
  int i, size;

  printf("enter array size ?\n");
  scanf("%d", &size);
  printf("\nEnter %d Elements for Sorting\n", size);
  for (i = 0; i < size; i++)
    scanf("%d", &arr[i]);

  printf("\nYour Data   :");
  for (i = 0; i < size; i++) {
    printf("\t%d", arr[i]);
  }

  merge_sort(0, size - 1);

  printf("\n\nSorted Data :");
  for (i = 0; i < size; i++) {
    printf("\t%d", arr[i]);
  }
  getch();

}

void merge_sort(int i, int j) {
  int m;

  if (i < j) {
    m = (i + j) / 2;
    merge_sort(i, m);
    merge_sort(m + 1, j);
    // Merging two arrays
    merge_array(i, m, m + 1, j);
  }
}

void merge_array(int a, int b, int c, int d) {
  int t[50];
  int i = a, j = c, k = 0;

  while (i <= b && j <= d) {
    if (arr[i] < arr[j])
      t[k++] = arr[i++];
    else
      t[k++] = arr[j++];
  }

  //collect remaining elements 
  while (i <= b)
    t[k++] = arr[i++];

  while (j <= d)
    t[k++] = arr[j++];

  for (i = a, j = 0; i <= d; i++, j++)
    arr[i] = t[j];
}